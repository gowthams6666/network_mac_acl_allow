import telnet_v0
from netaddr import *


user_name = raw_input('Username:')
Password = raw_input('Password:')
mac_fr = []
mac_d = []
ok = False
while ok == False:
    m = raw_input('Enter Mac address :\n')
    if len(m) > 12:
        mac_fr.append(m)
    else:
        ok = True
    
cisco_hosts = ['192.168.176.188','192.168.176.185','192.168.176.186','192.168.176.189','192.168.176.179','192.168.176.177','192.168.233.142']
juniper_host = ['192.168.176.190','192.168.176.178']

try :
    for m in mac_fr:
        mac_a = EUI(m)
        mac_a.dialect = mac_unix
        mac_d.append(str(mac_a))
        
    if len(mac_d) > 0:
        
        f = open('cisco_mac_tmp.txt', 'w+')
        f.write("<<Username|login\n>"+user_name+'\n<Password\n>'+Password+'\n')
        f.write("<#\n")
        f.write('>conf t\n')
        f.write("<#\n")
        f.write('>mac access-list extended NOC_USER\n')
        f.write("<#\n")
        for m in mac_d:
            if len(m) > 11:
                f.write('>permit host '+m+' any\n')
        f.write('>end\n<#\n>wr\n<#\n')
        f.close()
         
         
        f = open('juniper_mac_tmp.txt', 'w+')
        f.write("<<Username|login\n>"+user_name+'\n<Password\n>'+Password+'\n')
        f.write("<>\n")
        f.write(">edit\n")
        f.write("<#\n")
        for m in mac_d:
            if len(m) > 11:
                f.write('>set ethernet-switching-options secure-access-port interface MAC-bind allowed-mac '+m+'\n')
        f.write(">commit\n")
        f.write("<#\n>exit\n")
        f.close()
        
        for c in cisco_hosts:
            t = telnet_v0.telnet_session(c,10,'cisco_mac_tmp.txt','cisco_mac_output.txt')
            t.run()
            #raw_input('Press Block...')

        for j in juniper_host:
            t = telnet_v0.telnet_session(j,10,'juniper_mac_tmp.txt','juniper_mac_output.txt')
            t.run()
            #raw_input('Press Block...')
except Exception as e:
    print e
